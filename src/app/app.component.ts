import { Component } from '@angular/core';

import { OwlOptions } from 'ngx-owl-carousel-o';
import { TranslateService } from '@ngx-translate/core';
import { OverridesService } from './shared/overrides.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'project';
  constructor(
    private traslate: TranslateService,
    private override: OverridesService
  ){
    // this.traslate.setDefaultLang(
    //   this.override.currentLang ? this.override.currentLang : (this.traslate.getBrowserLang() == 'ar' ||
    //   this.traslate.getBrowserLang() == 'en' ? this.traslate.getBrowserLang() : 'en'))
    this.traslate.use(override.currentLang);
    this.override.changeLanguage( this.override.currentLang ? this.override.currentLang : 'en' );

  }


}
