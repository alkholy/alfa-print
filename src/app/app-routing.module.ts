import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', loadChildren: () => import('./landing-page/landing-page.module').then(m => m.LandingPageModule) },
 { path: 'blog', loadChildren: () => import('./landing-page/blog/blog.module').then(m => m.BlogModule) },
  { path: 'article', loadChildren: () => import('./landing-page/article/article.module').then(m => m.ArticleModule) },
  { path: 'contact-us', loadChildren: () => import('./landing-page/contact-us/contact-us.module').then(m => m.ContactUsModule) },
  { path: 'projects', loadChildren: () => import('./landing-page/projects/projects.module').then(m => m.ProjectsModule) },
  { path: 'about-us', loadChildren: () => import('./landing-page/about-us/about-us.module').then(m => m.AboutUsModule) },
  { path: 'items', loadChildren: () => import('./landing-page/items/items.module').then(m => m.ItemsModule) }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
