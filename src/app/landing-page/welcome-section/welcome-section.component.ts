import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from './../../shared/seo.service';

@Component({
  selector: 'app-welcome-section',
  templateUrl: './welcome-section.component.html',
  styleUrls: ['./welcome-section.component.scss']
})
export class WelcomeSectionComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }

  public welcomeData;
  ngOnInit(): void {
    this.firbase.get('welcome').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });

      this.welcomeData = data[0];
      // console.log(this.welcomeData);

          // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.welcomeData.texthead1[this.override.currentLang], this.welcomeData.body1[this.override.currentLang] , { tags: 'Alfa Print, Printify , Print' } );
      this.seo.updateMetaInfo(this.welcomeData.texthead1[this.override.currentLang], this.welcomeData.body1[this.override.currentLang] );
    });

  }

}
