import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  public aboutData;

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService,
    private translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.firbase.get('aboutUs').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.aboutData = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.aboutData.titlepage[this.override.currentLang], this.aboutData.aboutDescription[this.override.currentLang] , { tags: this.aboutData.howWorksTitle } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.aboutData.titlepage[this.override.currentLang], this.aboutData.aboutDescription[this.override.currentLang] );
    });

  }
}

