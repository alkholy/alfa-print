import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
  public clientsData;
  ngOnInit(): void {
    this.firbase.get('clients').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });


      this.clientsData = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.clientsData.clients[0].quote_en, this.clientsData.clients[0].quote_en , { tags: 'Alfa Print, Printify , Print, 3D Printing' } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.clientsData.clients[0].quote_ar, this.clientsData.clients[0].quote_ar );
    });
  }

}
