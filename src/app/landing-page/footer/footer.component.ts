import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {


  constructor(
    private firbase: FirebaseService,
    public override: OverridesService
  ) { }
    public footerData;
  ngOnInit(): void {
    this.firbase.get('footer').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.footerData = data[0];

    });
  }
}
