import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  constructor() { }

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 700,  // sor3t n2l ben sora lel tania
    navText: ['Previous', 'Next'],
    autoplay: true,
    autoplayTimeout: 2000,
    autoplayHoverPause: false,
    // margin:10,
    animateOut: 'fadeOut',
    responsive: {
      0: {
        items: 1
      },
      // 400: {
      //   items: 2
      // },
      // 740: {
      //   items: 3
      // },
      // 940: {
      //   items: 4
      // }
    },
    nav: true
  };
    // Images = [
  //   'https://cache.desktopnexus.com/thumbseg/2003/2003684-bigthumbnail.jpg',
  //   'https://www2.ciimar.up.pt/imgs/settings/imgs_ucznq_ref.imgs_trzp5_ref.pexels-photo-355288.jpeg',
  //   'https://www2.ciimar.up.pt/imgs/settings/imgs_frs3s_ref.pexels-photo-391522_1.jpg'
  // ];

  // SlideOptions = { items: 1, dots: true, nav: true };
  // CarouselOptions = { items: 3, dots: true, nav: true };


  ngOnInit(): void {
  }

}
