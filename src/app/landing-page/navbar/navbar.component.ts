import { Component, OnInit } from '@angular/core';
import { OverridesService } from 'src/app/shared/overrides.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    public override: OverridesService,
    private translate: TranslateService
  ) { }

  public navItemList = [

    { link: true, input: false, dropdown: false, href: '/', title: this.translate.instant('Home'), class: 'btn-light active', icon: '' },
    { link: true, input: false, dropdown: false, href: '/about-us', title: this.translate.instant('About'), class: 'btn-light', icon: '' },
    {
      link: false, input: false, dropdown: true,
      droplist:
        [
          { href: '/items/displayItems', listTitle: this.translate.instant('purchase order'), class: 'btn-light', icon: '' },
          // { href: '/', listTitle: this.translate.instant('Category two'), class: 'btn-light', icon: '',  },
        ],
      title: this.translate.instant('Services'),
      class: 'btn-light',
      icon: '',

    },
    // { link: true, input: false, dropdown: false, href: '/blog', title: this.translate.instant('Blogs'), class: 'btn-light', icon: '' },

    {
      link: false, input: false, dropdown: true,
      droplist:
        [
          { href: '/blog', listTitle: this.translate.instant('Blogs'), class: 'btn-light', icon: '' },
          // { href: '/article', listTitle: this.translate.instant('Article'), class: 'btn-light', icon: '',  },
          { href: '/contact-us', listTitle: this.translate.instant('Contact'), class: 'btn-light', icon: '',  },
          { href: '/projects', listTitle: this.translate.instant('Projects'), class: 'btn-light', icon: '',  },
        ],
      title: this.translate.instant('Pages'),
      class: 'btn-light',
      icon: '',
      target: '_parent',
    },
    {
      link: false, input: false, dropdown: true,
      droplist:
        [
          {  listTitle: 'English', class: 'btn-light', icon: '', val: 'en' },
          {  listTitle: 'Arabic', class: 'btn-light', icon: '', val : 'ar'},
        ],
      title: 'English',
      class: 'btn-light',
      icon: 'fas fa-globe-americas',
      target: '_parent',
    },
    // {
    //   link: false, input: false, dropdown: true,
    //   droplist:
    //     [
    //       { href: '/', listTitle: 'Shop Page', class: 'btn-light', icon: '' },
    //       { href: '/', listTitle: 'Shop Page', class: 'btn-light', icon: '',  },
    //       { href: '/', listTitle: 'Shop Page', class: 'btn-light', icon: '',  },
    //     ],
    //   title: 'Shop',
    //   class: 'btn-light',
    //   icon: '',
    //   target: '_parent',
    // },
    // {
    //   link: false, input: false, dropdown: true,
    //   droplist:
    //     [
    //       { href: '/', listTitle: 'Category one', class: 'btn-light', icon: '' },
    //       { href: '/', listTitle: 'Category two', class: 'btn-light', icon: '',  },
    //       { href: '/', listTitle: 'Category two', class: 'btn-light', icon: '',  },
    //       { href: '/', listTitle: 'Category two', class: 'btn-light', icon: '',  },
    //       { href: '/', listTitle: 'Category two', class: 'btn-light', icon: '',  },
    //     ],
    //   title: 'News',
    //   class: 'btn-light',
    //   icon: '',
    //   target: '_parent',
    // },

  ];

  ngOnInit(): void {
  }

}
