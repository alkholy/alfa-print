import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';

import { ContactUsRoutingModule } from './contact-us-routing.module';
import { ContactUsComponent } from './contact-us.component';
import { LandingPageModule } from '../landing-page.module';

import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [ContactUsComponent],
  imports: [
    CommonModule,
    ContactUsRoutingModule,
    LandingPageModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),

  ]
})
export class ContactUsModule { }
