import { Component, OnInit } from '@angular/core';
import { OverridesService } from 'src/app/shared/overrides.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  constructor(
    public override: OverridesService,
    private fb: FormBuilder,
    private firestore: FirebaseService,
    private toastr: ToastrService

  ) { }
  form: FormGroup = new FormGroup({});
  ngOnInit(): void {
    this.form = this.fb.group({
      name: this.fb.control('', [Validators.required]),
      email: this.fb.control('', [Validators.required, Validators.email]),
      phone: this.fb.control('', [Validators.required, Validators.pattern(this.override.phoneNumberRegExp)]),
      message: this.fb.control('', [Validators.required]),
    });
  }

  // tslint:disable-next-line: typedef
  onSubmit(){
    if (this.form.invalid) {
      return;
    }
    this.firestore.create(this.form.value, 'messageRequests').then(res => {
      this.form.reset();
      this.toastr.success('Message has been sent Successfully', 'Message');
    });
  }
}
