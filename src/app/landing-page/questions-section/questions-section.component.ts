import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-questions-section',
  templateUrl: './questions-section.component.html',
  styleUrls: ['./questions-section.component.scss']
})
export class QuestionsSectionComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
    public questions;
  ngOnInit(): void {
    this.firbase.get('questions').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.questions = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.questions.texthead[this.override.currentLang], this.questions.description[this.override.currentLang] , { tags: 'Alfa Print, Printify , Print, 3D Printing' } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.questions.texthead[this.override.currentLang], this.questions.description[this.override.currentLang] );

    });
  }

}
