import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-benefits-section',
  templateUrl: './benefits-section.component.html',
  styleUrls: ['./benefits-section.component.scss']
})
export class BenefitsSectionComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
    public benefitsData;
  ngOnInit(): void {
    this.firbase.get('benefits').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.benefitsData = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.benefitsData.title1[this.override.currentLang], this.benefitsData.title2[this.override.currentLang] , { tags: this.benefitsData.benefits[0].benefit_en  } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.benefitsData.title1[this.override.currentLang], this.benefitsData.title2[this.override.currentLang] );

    });
  }

}
