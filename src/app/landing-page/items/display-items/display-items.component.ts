import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AngularFireUploadTask, AngularFireStorage } from '@angular/fire/storage';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { Observable } from 'rxjs';
import { PaymentService } from '../payment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-display-items',
  templateUrl: './display-items.component.html',
  styleUrls: ['./display-items.component.scss']
})
export class DisplayItemsComponent implements OnInit {
  form:FormGroup = new FormGroup({});
  constructor(
    public override: OverridesService,
    private firbase: FirebaseService,
    private seo: SeoService,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private storage: AngularFireStorage,
    private paymentService: PaymentService,
    private router: Router,

  ) { }
    public items;
    selectedItem: FormControl = new FormControl('');
    itemYouSelect ;

  ngOnInit(): void {
    this.form = this.fb.group({
      name: this.fb.control(null, [Validators.required]),
      email: this.fb.control(null, [Validators.required, Validators.email]),
      phone: this.fb.control(null, [Validators.required, Validators.pattern(this.override.phoneNumberRegExp)]),
      quantity: this.fb.control(0, [Validators.required]),
      total: this.fb.control(0, [Validators.required]),
      item: this.fb.control(null),
      file: this.fb.control(null),
      status: this.fb.control('pendening')
    });
    this.form.get('total').disable();
    this.firbase.get('items').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.items = data[0];
      // tslint:disable-next-line: max-line-length
      // tslint:disable-next-line: no-string-literal
      this.seo.updateMetaInfo(this.items.items[0]['itemName_en'], this.items.items[0]['itemName_en'] , { tags: this.items.items[0]['itemName_en'] } );
      // tslint:disable-next-line: max-line-length
      // tslint:disable-next-line: no-string-literal
      this.seo.updateMetaInfo(this.items.items[0]['itemName_ar'], this.items.items[0]['itemName_ar'] );
    });

    this.selectedItem.valueChanges.subscribe(data => {
      // this.quantity.setValue(0);
      // this.total.setValue(0);
      const ce =  this.items.items.findIndex((item, index) => {
        return data == item.id;
      });
      // console.log(this.items.items[ce]);
      this.itemYouSelect = this.items.items[ce];
      this.form.get('quantity').setValue(0);
        this.form.get('total').setValue(0);
      // return data;
    });

    this.form.get('quantity').valueChanges.subscribe(qu => {
      this.form.get('total').setValue(+qu * +this.itemYouSelect.priceOfM) ;
    });
  }
  // tslint:disable-next-line: member-ordering
  task: AngularFireUploadTask;
  percentage: Observable<number> = new Observable<number>();
  // tslint:disable-next-line: member-ordering
  downloadURL: string;
  fileType = '';

  async onFilePicked(event) {
    const file = event.target.files[0];
    let type = file.type as string;

    let TypeToSave = checkFileType(type);
    this.fileType = TypeToSave;
    if (this.form.get('file')) {
      this.form.get('file').setValue(TypeToSave);
    }

    // The storage path
    const path = `${this.form.get('file').value}/${Date.now()}_${file.name}`;
    // Reference to storage bucket
    const ref = this.storage.ref(path);
    // The main task
    const customMetadata = { app: 'alfaprint' };
    this.task = this.storage.upload(path, file, { customMetadata });
    // Progress monitoring
    this.percentage = this.task.percentageChanges();
    this.downloadURL = await (await this.task).ref.getDownloadURL();
    this.form.controls['file'].setValue(
      await (await this.task).ref.getDownloadURL()
    );
  }
  logf(){
    console.log(this.form);

  }

  submit(){

    this.form.get('item').setValue(this.itemYouSelect);
    // console.log(this.form.getRawValue());
    const request = this.form;
    this.paymentService.setPaymentForm(request);
    this.router.navigate(['items', 'checkOut']);
  }
}
function checkFileType(type: String) {
  let TypeToSave = '';
  if (type.indexOf('image') >= 0) { TypeToSave = 'image'; }
  if (type.indexOf('pdf') >= 0) { TypeToSave = 'pdf'; }
  if (type.indexOf('video') >= 0) { TypeToSave = 'video'; }
  if (
    type.indexOf('STL') >= 0 ||
    type.indexOf('OBJ') >= 0 ||
    type.indexOf('FBX') >= 0 ||
    type.indexOf('COLLADA') >= 0 ||
    type.indexOf('3DS') >= 0 ||
    type.indexOf('IGES') >= 0
  ) {
    TypeToSave = 'threeDimensionImage';
  }

  return TypeToSave;
}
