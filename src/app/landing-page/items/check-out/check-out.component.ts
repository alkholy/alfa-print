import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OverridesService } from 'src/app/shared/overrides.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder } from '@angular/forms';
declare let Tapjsli: any;
import { PaymentService } from './../payment.service';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { ToastrService } from 'ngx-toastr';
import { AngularFireStorage } from '@angular/fire/storage';
import { noUndefined } from '@angular/compiler/src/util';

const ELEMENT_DATA = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];


@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.scss']
})
export class CheckOutComponent implements OnInit {

  paymentForm: FormGroup;
  displayedColumns: string[] = [ 'name', 'email', 'phone', 'quantity', 'file', 'total'];
  dataSource: any[] = [] ;
  accountsManger;
  constructor(
    public override: OverridesService,
    private router: Router,
    private translate: TranslateService,
    private paymentService: PaymentService,
    private firbase: FirebaseService,
    private fb: FormBuilder,
    private toaster: ToastrService,
    private storage: AngularFireStorage,
  ) { }

  ngOnInit(): void {
    console.log(this.paymentForm);
    setTimeout(() => {
      if (this.paymentForm == undefined ) {
        this.router.navigate(['/items', 'displayItems']);
      }
    }, 2000);

    this.firbase.get('accountManger').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data);

      this.accountsManger = data;
    });

    this.paymentForm = this.paymentService.getPaymentForm();
    console.log(this.paymentForm, this.paymentForm.getRawValue());
    this.dataSource.push(this.paymentForm.getRawValue());
    console.log(this.dataSource);


  }

  // tslint:disable-next-line: typedef
  pay() {

    this.firbase.create(this.paymentForm.getRawValue() , 'requestPurchase').then(res => {
      // console.log(res);
      this.toaster.success( this.translate.instant('Your Request has been sent successfully'), this.translate.instant('Parchase Request'));
      this.router.navigate(['/']);

    }).catch(err => {
      console.log(err);

      this.toaster.error(err, this.translate.instant('Parchase Request'));

    });
  }



}
