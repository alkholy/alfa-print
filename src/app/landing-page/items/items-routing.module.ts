import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ItemsComponent } from './items.component';
import { DisplayItemsComponent } from './display-items/display-items.component';
import { CheckOutComponent } from './check-out/check-out.component';

const routes: Routes = [
  { path: '', component: ItemsComponent, children: [
    {path: '' , redirectTo: 'displayItems', pathMatch: 'full'},
    {path: 'checkOut' , component: CheckOutComponent },
    {path: 'displayItems' , component: DisplayItemsComponent},
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemsRoutingModule { }
