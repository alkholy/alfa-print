import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  paymentForm: FormGroup
  constructor() { }

  setPaymentForm(value : FormGroup) {
    this.paymentForm = value;
  }
  getPaymentForm(){
    return this.paymentForm;
  }


}
