import { Component, OnInit } from '@angular/core';
import { OverridesService } from 'src/app/shared/overrides.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  constructor(
    public override: OverridesService
  ) { }

  ngOnInit(): void {
  }

}
