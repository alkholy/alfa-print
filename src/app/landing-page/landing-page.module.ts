import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingPageRoutingModule } from './landing-page-routing.module';
import { LandingPageComponent } from './landing-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { LandingSectionComponent } from './landing-section/landing-section.component';
import { CarouselComponent } from './carousel/carousel.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { OwlModule } from 'ngx-owl-carousel';
import { WelcomeSectionComponent } from './welcome-section/welcome-section.component';
import { FeaturesComponent } from './features/features.component';
import { ClientsComponent } from './clients/clients.component';
import { QuestionsSectionComponent } from './questions-section/questions-section.component';
import { ServicesSectionComponent } from './services-section/services-section.component';
import { BenefitsSectionComponent } from './benefits-section/benefits-section.component';
import { CardsSectionComponent } from './cards-section/cards-section.component';
import { ImgDarkCardComponent } from './img-dark-card/img-dark-card.component';
import { RecentProjectsComponent } from './recent-projects/recent-projects.component';
import { PaginationComponent } from './pagination/pagination.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
      LandingPageComponent,
      NavbarComponent,
      FooterComponent,
      LandingSectionComponent,
      CarouselComponent,
      WelcomeSectionComponent,
      FeaturesComponent, ClientsComponent,
      QuestionsSectionComponent,
      ServicesSectionComponent,
      BenefitsSectionComponent,
      CardsSectionComponent,
      ImgDarkCardComponent,
      RecentProjectsComponent,
      PaginationComponent,
      SideBarComponent,
      ],
  exports: [
    NavbarComponent,
    FooterComponent,
    SideBarComponent
  ],
    imports: [
        CommonModule,
        LandingPageRoutingModule,
        CarouselModule,
        OwlModule,
        TranslateModule.forChild()
    ]
})
export class LandingPageModule { }
