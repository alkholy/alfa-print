import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-cards-section',
  templateUrl: './cards-section.component.html',
  styleUrls: ['./cards-section.component.scss']
})
export class CardsSectionComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
    public CardsData;
  ngOnInit(): void {
    this.firbase.get('cards').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.CardsData = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.CardsData.titleCard1[this.override.currentLang], this.CardsData.titleCard2[this.override.currentLang] , { tags: [this.CardsData.designTypes[0].name_en, this.CardsData.designTypes[1].name_en, this.CardsData.designTypes[2].name_en] } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.CardsData.titleCard1[this.override.currentLang], this.CardsData.titleCard2[this.override.currentLang] );
    });
  }

}
