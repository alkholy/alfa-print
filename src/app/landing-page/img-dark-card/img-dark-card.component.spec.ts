import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgDarkCardComponent } from './img-dark-card.component';

describe('ImgDarkCardComponent', () => {
  let component: ImgDarkCardComponent;
  let fixture: ComponentFixture<ImgDarkCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImgDarkCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImgDarkCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
