import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-img-dark-card',
  templateUrl: './img-dark-card.component.html',
  styleUrls: ['./img-dark-card.component.scss']
})
export class ImgDarkCardComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
    public imgDark;
  ngOnInit(): void {
    this.firbase.get('imgDark').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.imgDark = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.imgDark.headtext[this.override.currentLang], this.imgDark.bodyCard2[this.override.currentLang] , { tags: this.imgDark.titleCard1[this.override.currentLang] } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.imgDark.headtext[this.override.currentLang], this.imgDark.bodyCard2[this.override.currentLang] );
    });
  }

}
