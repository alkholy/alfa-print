import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-recent-projects',
  templateUrl: './recent-projects.component.html',
  styleUrls: ['./recent-projects.component.scss']
})
export class RecentProjectsComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
    public resentProjects;
  ngOnInit(): void {
    this.firbase.get('resentProjects').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.resentProjects = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.resentProjects.headsectiontitle1[this.override.currentLang], this.resentProjects.body[this.override.currentLang] , { tags: this.resentProjects.projects[0].projectName_en } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.resentProjects.headsectiontitle1[this.override.currentLang], this.resentProjects.body[this.override.currentLang] );

    });
  }

}
