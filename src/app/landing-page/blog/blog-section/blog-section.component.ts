import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { Subject } from 'rxjs';
import { SeoService } from 'src/app/shared/seo.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-blog-section',
  templateUrl: './blog-section.component.html',
  styleUrls: ['./blog-section.component.scss']
})
export class BlogSectionComponent implements OnInit {


  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService,
    private translate: TranslateService
  ) { }
  totalRecords: number;
  page = 1;
  config: any;
  Previous: string = this.translate.instant('Previous');
  Next: string = this.translate.instant('Next');
    public  blogs;
  ngOnInit(): void {
    this.firbase.get('blogs').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.blogs = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.blogs.titleSection[this.override.currentLang], this.blogs.blogtitle[this.override.currentLang] , { tags: this.blogs.blogs[0].title_en } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.blogs.titleSection[this.override.currentLang], this.blogs.blogtitle[this.override.currentLang] );
    });

    this.config = {
      itemsPerPage: 2,
      currentPage: 1,
      totalItems: this.blogs?.blogs.length
    };
  }
  public pageChanged(event) {
    this.config.currentPage = event;
  }


}
