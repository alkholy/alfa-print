import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { LandingPageModule } from '../landing-page.module';
import { BlogSectionComponent } from './blog-section/blog-section.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [BlogComponent, BlogSectionComponent],
  imports: [
    CommonModule,
    BlogRoutingModule,
    LandingPageModule,
    NgxPaginationModule,
    TranslateModule.forChild()
  ]
})
export class BlogModule { }
