import { Component, OnInit, Input } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {
  blogs;
  items;


  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService,
  ) { }

  ngOnInit(): void {
    this.firbase.get('blogs').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.blogs = data[0];
    });

    this.firbase.get('items').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      console.log(data[0]);

      this.items = data[0];
      // tslint:disable-next-line: max-line-length
      // tslint:disable-next-line: no-string-literal
      this.seo.updateMetaInfo(this.items.items[0]['itemName_en'], this.items.items[0]['itemName_en'] , { tags: this.items.items[0]['itemName_en'] } );
      // tslint:disable-next-line: max-line-length
      // tslint:disable-next-line: no-string-literal
      this.seo.updateMetaInfo(this.items.items[0]['itemName_ar'], this.items.items[0]['itemName_ar'] );
    });
  }

}
