import { OverridesService } from './../../shared/overrides.service';
import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './../../shared/firebase.service';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { SeoService } from './../../shared/seo.service';

@Component({
  selector: 'app-landing-section',
  templateUrl: './landing-section.component.html',
  styleUrls: ['./landing-section.component.scss']
})
export class LandingSectionComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
    public sliderData;
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: true,
    navSpeed: 2000,  // sor3t n2l ben sora lel tania
    navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
    autoplay: true,
    autoplayTimeout: 5500,
    autoplayHoverPause: false,
    // margin:10,
    animateOut: 'fadeOut',
    responsive: {
      0: {
        items: 1
      },
      // 400: {
      //   items: 2
      // },
      // 740: {
      //   items: 3
      // },
      // 940: {
      //   items: 4
      // }
    },
    nav: true
  };

  ngOnInit(): void {
    this.firbase.get('slider').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });

      this.sliderData = data;
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.sliderData[0].title1[this.override.currentLang], this.sliderData[0].title2[this.override.currentLang] , { tags: 'Alfa Print, Printify ' } );
      this.seo.updateMetaInfo(this.sliderData[0].title1[this.override.currentLang], this.sliderData[0].title2[this.override.currentLang] );
    });
  }
}
