import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
  public featuresData: any;
  othersCards;
  firstCard;
  ngOnInit(): void {
    this.firbase.get('features').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      console.log(data[0]);

      this.featuresData = data[0];
      // this.firstCard = this.featuresData?.cards[0];
      // this.othersCards = this.featuresData?.cards.splice(1, 2);
          // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.featuresData.texthead1[this.override.currentLang], this.featuresData.texthead2[this.override.currentLang] , { tags: 'Alfa Print, Printify , Print, 3D Printing' } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.featuresData.texthead1[this.override.currentLang], this.featuresData.texthead2[this.override.currentLang] );
      this.seo.updateMetaInfo(this.firstCard.text_ar, this.firstCard.bodycard_ar );
    });




  }

}
