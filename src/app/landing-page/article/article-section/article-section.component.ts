import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';

@Component({
  selector: 'app-article-section',
  templateUrl: './article-section.component.html',
  styleUrls: ['./article-section.component.scss']
})
export class ArticleSectionComponent implements OnInit {

  constructor(
    private activeRoute: ActivatedRoute,
    private firbase: FirebaseService,
    public override: OverridesService,

  ) { }
    blogDate;
  ngOnInit(): void {
    this.activeRoute.paramMap.subscribe(param => {
      // console.log(param);

      this.firbase.getOne(param.get('id'), 'blogs').subscribe(res => {
        this.blogDate = res['blogs'][param.get('index')];
        // console.log('res ', this.blogDate);
      });
    });
  }

}
