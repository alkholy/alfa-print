import { OverridesService } from 'src/app/shared/overrides.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  constructor(
    public override: OverridesService
  ) { }

  ngOnInit(): void {
  }

}
