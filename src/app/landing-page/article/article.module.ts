import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleRoutingModule } from './article-routing.module';
import { ArticleComponent } from './article.component';
import { LandingPageModule } from '../landing-page.module';
import { BlogModule } from '../blog/blog.module';
import { ArticleSectionComponent } from './article-section/article-section.component';


@NgModule({
  declarations: [ArticleComponent, ArticleSectionComponent],
  imports: [
    CommonModule,
    ArticleRoutingModule,
    LandingPageModule,
    BlogModule
  ]
})
export class ArticleModule { }
