import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/shared/firebase.service';
import { OverridesService } from 'src/app/shared/overrides.service';
import { SeoService } from 'src/app/shared/seo.service';

@Component({
  selector: 'app-services-section',
  templateUrl: './services-section.component.html',
  styleUrls: ['./services-section.component.scss']
})
export class ServicesSectionComponent implements OnInit {

  constructor(
    private firbase: FirebaseService,
    public override: OverridesService,
    private seo: SeoService
  ) { }
    public servicesData;
  ngOnInit(): void {
    this.firbase.get('serveices').subscribe(res => {
      const data = res.map(e => {
        return Object.assign({ id: e.payload.doc.id }, e.payload.doc.data());
      });
      // console.log(data[0]);

      this.servicesData = data[0];
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.servicesData.title1[this.override.currentLang], this.servicesData.body1[this.override.currentLang] , { tags: this.servicesData.blocks[0].title_en } );
      // tslint:disable-next-line: max-line-length
      this.seo.updateMetaInfo(this.servicesData.title1[this.override.currentLang], this.servicesData.body1[this.override.currentLang] );

    });
  }

}
