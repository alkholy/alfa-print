import { Styles } from './styles';
import { Injectable } from '@angular/core';
import { Lang } from './lang.enum';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class OverridesService {

  constructor(
    private translate: TranslateService,
  ) { }
  get currentLang(): string {
    return localStorage.getItem('language');
  }


   // tslint:disable-next-line: typedef
   changeLanguage(val: string) {
    //  console.log(val);
     localStorage.setItem('language', val);
     this.translate.use(val);
     const htmlDocument =  document.getElementsByTagName('html')[0];
     htmlDocument.setAttribute('lang', val);
     if (val === 'en'){
       htmlDocument.setAttribute('dir', 'ltr');
       htmlDocument.dir = 'ltr';
      }else{
        htmlDocument.setAttribute('dir', 'rtl');
        htmlDocument.dir = 'rtl';

     }
     return true;
  }

  // tslint:disable-next-line: typedef
  switchLanguage(language: string, key?: string) {
    if (language in Lang) {
      if (this.currentLang != language) {
        this.translate.use(language);
        localStorage.setItem('language', language);

        this.setStyle();
      }
    } else {
      this.translate.use('en');
    }
  }

  // Style
  setStyle() {
    const keyLang = this.currentLang || this.translate.getDefaultLang();
    const styleFiles = Styles[keyLang];
    const htmlElement = document.getElementsByTagName('html')[0];
    const headElements = document.getElementsByTagName('head')[0];
    // const paras = document.getElementsByClassName(this.inverseLang(keyLang));
    // if (paras) {
    //   while (paras[0]) {
    //     paras[0].parentNode.removeChild(paras[0]);
    //   }
    // }
    htmlElement.setAttribute('lang', keyLang);
    if (keyLang == 'ar') {
      htmlElement.setAttribute('dir', 'rtl');
    } else {
      htmlElement.setAttribute('dir', 'ltr');
    }
    // for (let i = 0; i < styleFiles.length; i++) {
    //   const node = document.createElement("link");
    //   node.setAttribute("rel", "stylesheet");
    //   node.setAttribute("type", "text/css");
    //   node.setAttribute("href", Styles[keyLang][i]);
    //   // node.setAttribute('class', keyLang);
    //   headElements.appendChild(node);
    // }
  }
  isRTL(): boolean {
    const lang = localStorage.getItem('language');
    return lang ? lang === 'ar' : lang === 'en';
  }

  // tslint:disable-next-line: member-ordering
  phoneNumberRegExp = '(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))s*[)]?[-s.]?[(]?[0-9]{1,3}[)]?([-s.]?[0-9]{3})([-s.]?[0-9]{3,4})';

}
