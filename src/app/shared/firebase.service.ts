import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {


  constructor(private firestore: AngularFirestore) { }
  // tslint:disable-next-line: typedef
  create(object, objectName: string) {
    return this.firestore.collection(objectName).add(object);
  }
  // tslint:disable-next-line: typedef
  createSpecificId(object, objectName: string) {
    return this.firestore.collection(objectName).doc(`${object.id}`).set(object);
  }
  // tslint:disable-next-line: typedef
  get(objectName: string) {
    return this.firestore.collection(objectName).snapshotChanges();
  }
  // tslint:disable-next-line: typedef
  getOne(objectId: string, objectName: string) {
    return this.firestore.collection(objectName).doc(objectId).valueChanges();
  }
  // tslint:disable-next-line: typedef
  update(object, objectName: string) {
    return this.firestore.doc(objectName + '/' + object.id).set(object, { merge: true });
  }
  // tslint:disable-next-line: typedef
  delete(objectId: string, objectName: string) {
    this.firestore.doc(objectName + '/' + objectId).delete();
  }

  // tslint:disable-next-line: typedef
  getOneByPagination(objectName: string) {
    return  this.firestore.collection(objectName, ref => ref
      .limit(5)
    ).valueChanges();
  }

}
