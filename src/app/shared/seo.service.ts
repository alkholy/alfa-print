import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(
    private meta: Meta,
  ) { }


  updateMetaInfo(name: any, ...content: any[]) {
    content.forEach(element => {
      if (element && element.tags) {
        this.meta.addTag({ name, content: element, keyword: element.tags });
      }
      else {
        this.meta.addTag({ name, content: element });
      }
    });

  }

}
